function getExperienceDate(start, end) {
  const endDate = end === 'present' ? new Date() : end;

  let months;
  months = (endDate.getFullYear() - start.getFullYear()) * 12;
  months -= start.getMonth();
  months += endDate.getMonth() + 1;
  const years = Math.floor(months / 12);

  const format = date =>
    date === 'present'
      ? end
      : date.toLocaleDateString('en-EN', { month: 'short', year: 'numeric' });

  const range = `${format(start)} - ${format(end)}`;

  if (months <= 0) {
    return `${range} - 1 mos`;
  }

  if (months < 12) {
    return `${range} - ${months} mos`;
  }

  return `${range} - ${years} year${years > 1 ? 's' : ''} ${months % 12} mos`;
}

const companiesExperiences = [
  {
    link: 'https://apiko.com/',
    id: 'apiko',
    name: 'Apiko Software',
    role: 'Full Stack JS developer',
    start: new Date('03/15/2021'),
    end: 'present',
  },
  {
    link: 'https://www.smile.eu/',
    id: 'smile',
    role: 'QA Engineer',
    name: 'Smile Open Source Solutions',
    start: new Date('1 Aug 2019'),
    end: new Date('03/15/2021'),
  },
  {
    link: 'https://upwork.com/',
    id: 'upwork',
    name: 'Upwork',
    role: 'Freelance Web developer (Landing page development)',
    start: new Date('1 Apr 2019'),
    end: new Date('1 Jun 2019'),
  },
];

companiesExperiences.map(({ id, start, end, link, role, name }) => {
  const liElem = document.createElement('li');
  const aElem = document.createElement('a');
  const pElem = document.createElement('p');

  aElem.setAttribute('href', link);
  aElem.setAttribute('target', '_blank');
  aElem.setAttribute('rel', 'noreferrer noopener');
  aElem.textContent = name;

  const roleElem = document.createTextNode(` ${role}`);

  const experience = document.createTextNode(getExperienceDate(start, end));
  pElem.appendChild(experience);

  liElem.appendChild(aElem);
  liElem.appendChild(roleElem);
  liElem.appendChild(pElem);

  document.querySelector('#experience-list').appendChild(liElem);
});
